from django.urls import path

from . import views


urlpatterns = [
    # Index page
    path('',  views.index, name='index'),
]