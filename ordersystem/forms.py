from django import forms


# Форма для заказа
class OrderForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    phone_number = forms.CharField(max_length=15)
    content = forms.CharField()
